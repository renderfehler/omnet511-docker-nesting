FROM ubuntu:latest
MAINTAINER Robin Finkbeiner <finkbero@gmail.com>
LABEL Description="Docker image for Nesting Stupro University of Stuttgart"

# Install dependencies
RUN apt-get update && apt-get install -y \
    git \
    wget \
    build-essential \
    clang \
    bison \
    flex \
    perl \
    tcl-dev \
    tk-dev \
    libxml2-dev \
    zlib1g-dev \
    default-jre \
    doxygen \
    graphviz \
    libwebkitgtk-1.0-0 \
    xvfb \
    qt4-qmake \
    libqt4-dev \
    libqt4-opengl-dev \
    openscenegraph \
    libopenscenegraph-dev \
    openscenegraph-plugin-osgearth \
    osgearth  \
    osgearth-data \
    libosgearth-dev

# OMnet++ 5

# Create working directory
RUN mkdir -p /usr/omnetpp
WORKDIR /usr/omnetpp

# Fetch Omnet++ source
RUN wget https://omnetpp.org/omnetpp/send/30-omnet-releases/2314-omnetpp-5-1-1-linux
RUN tar xvfz omnetpp-5.1.1-src-linux.tgz

# Path
ENV PATH $PATH:/usr/omnetpp/omnetpp-5.1.1/bin

# Configure and compile
RUN cd omnetpp-5.1.1 && \
    xvfb-run ./configure && \
    make

# Cleanup
RUN apt-get clean && \
    rm -rf /var/lib/apt && \
    rm /usr/omnetpp/omnetpp-5.1.1-src-linux.tgz
